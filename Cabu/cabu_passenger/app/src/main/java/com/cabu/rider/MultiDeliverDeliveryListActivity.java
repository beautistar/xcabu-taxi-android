package com.cabu.rider;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.adapter.files.MultiDeliverAdapter;
import com.general.files.GeneralFunctions;
import com.general.files.StartActProcess;
import com.model.DeliveryDetails;
import com.cabu.rider.R;
import com.utils.Utils;
import com.view.CreateRoundedView;
import com.view.ErrorView;
import com.view.MButton;
import com.view.MTextView;
import com.view.MaterialRippleLayout;
import com.view.editBox.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by Admin on 17-02-2017.
 */
public class MultiDeliverDeliveryListActivity extends AppCompatActivity implements MultiDeliverAdapter.OnItemClickListener {
    public MultiDeliverAdapter multiDeliverAdapter;
    ArrayList<DeliveryDetails> list = new ArrayList<>();
    ErrorView errorView;
    ImageView backImgView;
    MTextView plusSignTxt;
    String userProfileJson = "";
    int sendReqBtnId;
    int addDetailsBtnId;
    ArrayList<String[]> list_paymentDoneBy_items = new ArrayList<>();
    android.support.v7.app.AlertDialog alert_paymentDoneByMain;
    android.support.v7.app.AlertDialog alert_paymentDoneBy;
    String paymenentDoneByRecipientId = "";
    String required_str = "";
    private CardView no_Delivery_Details_Area;
    private MTextView listIsEmpty_txt, titleTxt;
    private MButton btn_add_delivery_details, btn_send_request;
    private RecyclerView multiDeliverListRecyclerView;
    private ProgressBar loading_multi_deliver;
    private GeneralFunctions generalFunc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_multi_delivery_list);
        userProfileJson = getIntent().getStringExtra("userProfileJson");
        init();
        setLables();
    }

    private void init() {
        no_Delivery_Details_Area = (CardView) findViewById(R.id.no_Delivery_Details_Area);
        listIsEmpty_txt = (MTextView) findViewById(R.id.listIsEmpty_txt);
        titleTxt = (MTextView) findViewById(R.id.titleTxt);


        backImgView = (ImageView) findViewById(R.id.backImgView);
        multiDeliverListRecyclerView = (RecyclerView) findViewById(R.id.multiDeliverListRecyclerView);
        loading_multi_deliver = (ProgressBar) findViewById(R.id.loading_multi_deliver);
        btn_add_delivery_details = ((MaterialRippleLayout) findViewById(R.id.btn_add_delivery_details)).getChildView();
        btn_send_request = ((MaterialRippleLayout) findViewById(R.id.btn_send_request)).getChildView();
        btn_send_request.setBackgroundColor(getResources().getColor(R.color.appThemeColor_1));

        plusSignTxt = (MTextView) findViewById(R.id.plusSignTxt);
        plusSignTxt.setVisibility(View.GONE);

        errorView = (ErrorView) findViewById(R.id.errorView);
        generalFunc = new GeneralFunctions(getActContext());


        if (!generalFunc.retrieveValue("DeliverList").isEmpty()) {
            String data = generalFunc.retrieveValue("DeliverList");
            JSONArray deliveriesArr = generalFunc.getJsonArray("deliveries", data);
            if (deliveriesArr != null) {
                list.clear();
                for (int j = 0; j < deliveriesArr.length(); j++) {
                    JSONObject ja = generalFunc.getJsonObject(deliveriesArr, j);
                    DeliveryDetails deliveryDetails = new DeliveryDetails();
                    deliveryDetails.setRecipientId("" + generalFunc.getJsonValue("iRecipientId", ja.toString()));
                    deliveryDetails.setRecipientName("" + generalFunc.getJsonValue("RecipientName", ja.toString()));
                    deliveryDetails.setRecipientAddress("" + generalFunc.getJsonValue("RecipientAdd", ja.toString()));
                    deliveryDetails.setRecipientEmailAddress("" + generalFunc.getJsonValue("RecipientEmailAdd", ja.toString()));
                    deliveryDetails.setRecipientPhoneNumber("" + generalFunc.getJsonValue("RecipientPhnNo", ja.toString()));
                    deliveryDetails.setRecipientPhoneCode("" + generalFunc.getJsonValue("RecipientPhnCode", ja.toString()));
                    deliveryDetails.setAdditionalNotes("" + generalFunc.getJsonValue("RecipientAdditionalNote", ja.toString()));
                    deliveryDetails.setShippmentDetailTxt("" + generalFunc.getJsonValue("RecipientShipmentDetail", ja.toString()));
                    deliveryDetails.setDeleteDeliverDetailLbl("" + generalFunc.retrieveLangLBl("", "LBL_DELETE_DELIVERY_DETAILS_ALERT_TXT"));
                    deliveryDetails.setYesLbl("" + generalFunc.retrieveLangLBl("", "LBL_BTN_YES_TXT"));
                    deliveryDetails.setNoLbl("" + generalFunc.retrieveLangLBl("", "LBL_BTN_NO_TXT"));
                    deliveryDetails.setRecipientvLatitude("" + generalFunc.getJsonValue("vLattitude", ja.toString()));
                    deliveryDetails.setRecipientvLongitude("" + generalFunc.getJsonValue("vLongitude", ja.toString()));
                    deliveryDetails.setPackageTypeId("" + generalFunc.getJsonValue("iPackageTypeId", ja.toString()));
                    deliveryDetails.setvPackageTypeName("" + generalFunc.getJsonValue("vPackageTypeName", ja.toString()));
                    list.add(deliveryDetails);
                }
            }

        }
        setViews();

        addDetailsBtnId = Utils.generateViewId();
        btn_add_delivery_details.setBackgroundColor(getResources().getColor(R.color.appThemeColor_1));
        btn_add_delivery_details.setId(addDetailsBtnId);
        btn_add_delivery_details.setOnClickListener(new setOnClickList());

        backImgView.setOnClickListener(new setOnClickList());
        plusSignTxt.setOnClickListener(new setOnClickList());

    }


    private void setLables() {
        required_str = generalFunc.retrieveLangLBl("", "LBL_FEILD_REQUIRD_ERROR_TXT");
        titleTxt.setText(generalFunc.retrieveLangLBl("Delivery Details", "LBL_DELIVERY_DETAILS"));
//        btn_send_request.setText(generalFunc.retrieveLangLBl("Send Request", "LBL_SEND_REQ"));
        if (getIntent().getStringExtra("isDeliverNow") != null && getIntent().getStringExtra("isDeliverNow").equals("true")) {
            btn_send_request.setText(generalFunc.retrieveLangLBl("Send Request", "LBL_SEND_REQ"));
        } else {
            btn_send_request.setText(generalFunc.retrieveLangLBl("Confirm Booking", "LBL_CONFIRM_BOOKING"));
        }

        listIsEmpty_txt.setText(generalFunc.retrieveLangLBl("The list is empty.Tap on below button to add Delivery details.", "LBL_NO_DELIVERY_DEATILS_TXT"));
        btn_add_delivery_details.setText(generalFunc.retrieveLangLBl("Add Delivery Details", "LBL_ADD_DELIVERY_DETAILS_TXT"));
    }

    @Override
    public void onItemClickList(View v, int position) {

    }

    public void deleteDeliverDetails(String recipientId) {
        loading_multi_deliver.setVisibility(View.VISIBLE);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getRecipientId().equalsIgnoreCase(recipientId)) {
                    list.remove(i);
                    break;
                }
            }
        }
        setViews();
    }

    public void editDeliveryDetails(DeliveryDetails item) {

        Bundle bn = new Bundle();
        bn.putString("UserProfileJson", userProfileJson);
        bn.putSerializable("SelectedDetails", item);
        bn.putSerializable("list", list);

        for (Field field : item.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            Object value = null;
            try {
                value = field.get(item);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        new StartActProcess(getActContext()).startActForResult(EnterMultiDeliveryDetailActivity.class, bn, Utils.RIDE_DELIVERY_DETAILS_REQ_CODE);

    }

    public void selectPaymentDoneByDialog(final Bundle bn) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
        builder.setTitle("");

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.item_payment_done_by_cell, null);
        builder.setView(dialogView);

        MTextView paymentHintTxt = (MTextView) dialogView.findViewById(R.id.paymentHintTxt);
        MTextView submitArea = (MTextView) dialogView.findViewById(R.id.submitArea);
        final FrameLayout paymentDoneBySelectArea = (FrameLayout) dialogView.findViewById(R.id.paymentDoneBySelectArea);
        final MaterialEditText nameTypeBox = (MaterialEditText) dialogView.findViewById(R.id.nameTypeBox);

        paymentHintTxt.setText(generalFunc.retrieveLangLBl("CashPayment Done By", "LBL_CASH_PAYMENT_DONE_BY_TXT"));
        nameTypeBox.setHint("" + generalFunc.retrieveLangLBl("", "LBL_RECIPIENT_PAY_TXT"));
        submitArea.setText("" + generalFunc.retrieveLangLBl("Submit", "LBL_BTN_SUBMIT_TXT"));


        new CreateRoundedView(getActContext().getResources().getColor(R.color.appThemeColor_1), Utils.dipToPixels(getActContext(), 5), Utils.dipToPixels(getActContext(), 1),
                getActContext().getResources().getColor(R.color.appThemeColor_1), submitArea);

        Utils.removeInput(nameTypeBox);
        nameTypeBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadPaymentDoneBy(dialogView);

            }
        });


        nameTypeBox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP && !view.hasFocus()) {
                    loadPaymentDoneBy(dialogView);

                }
                return true;
            }
        });


        submitArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean nameSelected = Utils.checkText(nameTypeBox) ? true : Utils.setErrorFields(nameTypeBox, required_str);

                if (nameSelected == true) {
                    if (Utils.getText(nameTypeBox).toString().equalsIgnoreCase(generalFunc.retrieveLangLBl("By Me", "LBL_BY_ME_TXT")) && paymenentDoneByRecipientId.isEmpty()) {

                    } else {
                        bn.putString("recipientId", paymenentDoneByRecipientId);
                    }
                    closeMainAlertBox();
                    (new StartActProcess(getActContext())).setOkResult(bn);
                    finish();

                }
            }
        });

        alert_paymentDoneByMain = builder.create();
        if (generalFunc.isRTLmode() == true) {
            generalFunc.forceRTLIfSupported(alert_paymentDoneByMain);
        }
        alert_paymentDoneByMain.show();


    }

    public void showPaymentDoneBy() {
        if (alert_paymentDoneBy != null) {
            alert_paymentDoneBy.show();
        }
    }

    public void closeMainAlertBox() {
        if (alert_paymentDoneByMain != null) {
            alert_paymentDoneByMain.cancel();
        }
    }

    private void loadPaymentDoneBy(final View dialogView) {
        {
            list_paymentDoneBy_items.clear();

            ArrayList<String> items = new ArrayList<>();

            if (list != null) {

                String[] arr_str_data1 = new String[2];
                arr_str_data1[0] = "";
                arr_str_data1[1] = generalFunc.retrieveLangLBl("By Me", "LBL_BY_ME_TXT");

                list_paymentDoneBy_items.add(arr_str_data1);
                items.add(generalFunc.retrieveLangLBl("By Me", "LBL_BY_ME_TXT"));

                for (int i = 0; i < list.size(); i++) {

                    String[] arr_str_data = new String[2];
                    arr_str_data[0] = list.get(i).getRecipientId();
                    arr_str_data[1] = list.get(i).getRecipientName();

                    list_paymentDoneBy_items.add(arr_str_data);
                    items.add(list.get(i).getRecipientName());
                }


                CharSequence[] cs_currency_txt = items.toArray(new CharSequence[items.size()]);

                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
                builder.setTitle(generalFunc.retrieveLangLBl("Cash Payment Done By", "LBL_CASH_PAYMENT_DONE_BY_TXT"));
                builder.setItems(cs_currency_txt, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection

                        if (alert_paymentDoneBy != null) {
                            alert_paymentDoneBy.dismiss();
                        }

                        String RecipientId = list_paymentDoneBy_items.get(item)[0];

                        paymenentDoneByRecipientId = RecipientId;

                        ((MaterialEditText) dialogView.findViewById(R.id.nameTypeBox)).setText(list_paymentDoneBy_items.get(item)[1]);

                    }
                });


                alert_paymentDoneBy = builder.create();
                if (generalFunc.isRTLmode() == true) {
                    generalFunc.forceRTLIfSupported(alert_paymentDoneBy);
                }

                showPaymentDoneBy();
            }
        }
    }

    private Activity getActContext() {
        return MultiDeliverDeliveryListActivity.this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.RIDE_DELIVERY_DETAILS_REQ_CODE && resultCode == RESULT_OK && data != null) {
            list = new ArrayList<DeliveryDetails>();
            list = (ArrayList<DeliveryDetails>) data.getSerializableExtra("list");
            for (int i = 0; i < list.size(); i++) {
                for (Field field : list.get(i).getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    String name = field.getName();
                    Object value = null;
                    try {
                        value = field.get(list.get(i));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }

            setViews();
        }
    }

    private void setViews() {
        loading_multi_deliver.setVisibility(View.GONE);
        if (list.size() > 0) {

            multiDeliverListRecyclerView.setVisibility(View.VISIBLE);
            no_Delivery_Details_Area.setVisibility(View.GONE);
            btn_send_request.setVisibility(View.VISIBLE);
            plusSignTxt.setVisibility(View.VISIBLE);

            /*if (multiDeliverAdapter!=null) {
                multiDeliverAdapter.notifyDataSetChanged();
            }
            else
            {*/
            sendReqBtnId = Utils.generateViewId();
            btn_send_request.setId(sendReqBtnId);
            btn_send_request.setOnClickListener(new setOnClickList());


            multiDeliverAdapter = new MultiDeliverAdapter(getActContext(), list, generalFunc, false);
            multiDeliverListRecyclerView.setAdapter(multiDeliverAdapter);
            multiDeliverAdapter.setOnItemClickListener(this);
            multiDeliverAdapter.notifyDataSetChanged();
//            }
        } else {
            multiDeliverListRecyclerView.setVisibility(View.GONE);
            btn_send_request.setVisibility(View.GONE);
            no_Delivery_Details_Area.setVisibility(View.VISIBLE);
            plusSignTxt.setVisibility(View.GONE);
            if (multiDeliverAdapter != null)
                multiDeliverAdapter.notifyDataSetChanged();
        }
    }

    public class setOnClickList implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int i = view.getId();
            Utils.hideKeyboard(getActContext());
            if (i == btn_send_request.getId()) {
                Bundle bn = new Bundle();
                bn.putSerializable("list", list);
                bn.putString("recipientId", "");
                if (getIntent().getStringExtra("CashPayment") != null && !TextUtils.isEmpty(getIntent().getStringExtra("CashPayment")) && getIntent().getStringExtra("CashPayment").equalsIgnoreCase("true")) {

                    selectPaymentDoneByDialog(bn);
                } else {

                    (new StartActProcess(getActContext())).setOkResult(bn);
                    finish();
                }
            } else if (i == btn_add_delivery_details.getId()) {
                Bundle bn = new Bundle();
                bn.putString("UserProfileJson", userProfileJson);
                bn.putSerializable("list", list);
                new StartActProcess(getActContext()).startActForResult(EnterMultiDeliveryDetailActivity.class, bn, Utils.RIDE_DELIVERY_DETAILS_REQ_CODE);
            } else if (i == R.id.backImgView) {
                MultiDeliverDeliveryListActivity.super.onBackPressed();
            } else if (i == R.id.plusSignTxt) {
                Bundle bn = new Bundle();
                bn.putString("UserProfileJson", userProfileJson);
                bn.putSerializable("list", list);
                new StartActProcess(getActContext()).startActForResult(EnterMultiDeliveryDetailActivity.class, bn, Utils.RIDE_DELIVERY_DETAILS_REQ_CODE);

            }

        }
    }
}
