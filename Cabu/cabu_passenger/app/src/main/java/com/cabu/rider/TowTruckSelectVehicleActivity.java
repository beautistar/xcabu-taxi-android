package com.cabu.rider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.adapter.files.TowTruckVehicleAdpater;
import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.StartActProcess;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.GenerateAlertBox;
import com.view.MTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TowTruckSelectVehicleActivity extends AppCompatActivity implements TowTruckVehicleAdpater.OnItemClickListener {

    GeneralFunctions generalFunc;
    MTextView titleTxt;
    MTextView addVehicleTxtView;
    ImageView backImgView;
    TowTruckVehicleAdpater towTruckVehicleAdpater;
    RecyclerView vehiclesListRecyclerView;
    ProgressBar towTruckLoading;

    ArrayList<HashMap<String, String>> towTruckDriverList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow_truck_select_vehicle);
        generalFunc = new GeneralFunctions(getActContext());

        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        addVehicleTxtView = (MTextView) findViewById(R.id.addVehicleTxtView);
        addVehicleTxtView.setVisibility(View.VISIBLE);
        backImgView = (ImageView) findViewById(R.id.backImgView);
        vehiclesListRecyclerView = (RecyclerView) findViewById(R.id.vehiclesListRecyclerView);
        towTruckLoading = (ProgressBar) findViewById(R.id.towTruckLoading);
        backImgView.setOnClickListener(new setOnClickList());
        addVehicleTxtView.setOnClickListener(new setOnClickList());
        titleTxt.setText(generalFunc.retrieveLangLBl("Choose Vehicles", "LBL_CHOOSE_VEHICLE"));

        towTruckVehicleAdpater = new TowTruckVehicleAdpater(getActContext(), towTruckDriverList, generalFunc);
        vehiclesListRecyclerView.setAdapter(towTruckVehicleAdpater);
        towTruckVehicleAdpater.notifyDataSetChanged();
        towTruckVehicleAdpater.setOnClickList(this);
        getUserVehicles();
    }

    public void getUserVehicles() {
        towTruckLoading.setVisibility(View.VISIBLE);
        towTruckDriverList.clear();
        towTruckVehicleAdpater.notifyDataSetChanged();

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "displayuservehicles");
        parameters.put("iUserId", generalFunc.getMemberId());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
//        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {

                        JSONArray message_arr = generalFunc.getJsonArray(CommonUtilities.message_str, responseString);

                        for (int i = 0; i < message_arr.length(); i++) {
                            JSONObject obj_temp = generalFunc.getJsonObject(message_arr, i);

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("vTitle", generalFunc.getJsonValue("vTitle", obj_temp.toString()));
                            map.put("vMake", generalFunc.getJsonValue("vMake", obj_temp.toString()));
                            map.put("iUserVehicleId", generalFunc.getJsonValue("iUserVehicleId", obj_temp.toString()));
                            map.put("iYear", generalFunc.getJsonValue("iYear", obj_temp.toString()));
                            map.put("vColour", generalFunc.getJsonValue("vColour", obj_temp.toString()));

                            towTruckDriverList.add(map);
                        }
                        towTruckVehicleAdpater.notifyDataSetChanged();
                        towTruckLoading.setVisibility(View.GONE);
                    } else {
                        generalFunc.showGeneralMessage("",
                                generalFunc.retrieveLangLBl("", generalFunc.getJsonValue(CommonUtilities.message_str, responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public Context getActContext() {
        return TowTruckSelectVehicleActivity.this;
    }

    @Override
    public void setOnClick(String action, final int position) {
        Utils.hideKeyboard(getActContext());
        if (action.equalsIgnoreCase("delete")) {
            final GenerateAlertBox generateAlertBox = new GenerateAlertBox(getActContext());
            generateAlertBox.setContentMessage("", towTruckDriverList.get(position).get(""));

            generateAlertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                @Override
                public void handleBtnClick(int btn_id) {
                    if (btn_id == 1) {
//                        deleteItem(towTruckDriverList.get(position).get(""));
                    } else {
                        generateAlertBox.closeAlertBox();
                    }
                }
            });
            generateAlertBox.setPositiveBtn(towTruckDriverList.get(position).get("LBL_BTN_YES_TXT"));
            generateAlertBox.setNegativeBtn(towTruckDriverList.get(position).get("LBL_BTN_NO_TXT"));
            generateAlertBox.showAlertBox();
        } else if (action.equalsIgnoreCase("edit")) {
            final GenerateAlertBox generateAlertBox = new GenerateAlertBox(getActContext());
            generateAlertBox.setContentMessage("", towTruckDriverList.get(position).get(""));

            generateAlertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                @Override
                public void handleBtnClick(int btn_id) {
                    if (btn_id == 1) {
//                        editItem(towTruckDriverList.get(position).get(""));
                    } else {
                        generateAlertBox.closeAlertBox();
                    }
                }
            });
            generateAlertBox.setPositiveBtn(towTruckDriverList.get(position).get("LBL_BTN_YES_TXT"));
            generateAlertBox.setNegativeBtn(towTruckDriverList.get(position).get("LBL_BTN_NO_TXT"));
            generateAlertBox.showAlertBox();


        } else if (action.equalsIgnoreCase("Select")) {

            String iUserVehicleId = towTruckDriverList.get(position).get("iUserVehicleId");
            String SelectedVehicleTypeId = getIntent().getStringExtra("SelectedVehicleTypeId");
            String USER_PROFILE_JSON = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);
            String quantity = getIntent().getStringExtra("Quantity");

            if (quantity == null || quantity.equals("0")) {
                quantity = "1";
            }
            Bundle bn = new Bundle();
            bn.putString("USER_PROFILE_JSON", USER_PROFILE_JSON);
            bn.putString("iUserVehicleId", iUserVehicleId);
            bn.putString("SelectedVehicleTypeId", SelectedVehicleTypeId);
            bn.putString("Quantity", quantity);

            new StartActProcess(getActContext()).startActWithData(MainActivity.class, bn);

        }
    }


    public void deleteItem(String id) {

//        CALL DELETE WEBSERVICE

    }

    public void editItem(String id) {

//        CALL EDIT WEBSERVICE

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utils.ADD_VEHICLE_REQ_CODE && resultCode == RESULT_OK) {
            getUserVehicles();
        }
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int i = v.getId();
            Utils.hideKeyboard(getActContext());
            if (i == R.id.backImgView) {
                TowTruckSelectVehicleActivity.super.onBackPressed();
            } else if (i == R.id.addVehicleTxtView) {
                new StartActProcess(getActContext()).startActForResult(TowTruckAddVehicleActivity.class, Utils.ADD_VEHICLE_REQ_CODE);
            }
        }
    }
}
