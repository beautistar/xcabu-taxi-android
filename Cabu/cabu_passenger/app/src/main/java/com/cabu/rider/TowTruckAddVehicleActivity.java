package com.cabu.rider;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.SetOnTouchList;
import com.general.files.StartActProcess;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.CreateRoundedView;
import com.view.MButton;
import com.view.MTextView;
import com.view.MaterialRippleLayout;
import com.view.SelectableRoundedImageView;
import com.view.editBox.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class TowTruckAddVehicleActivity extends AppCompatActivity {

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "Temp";
    private static final int SELECT_PICTURE = 2;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    GeneralFunctions generalFunc;
    MTextView titleTxt;
    ImageView backImgView;
    MButton submitVehicleBtn;
    ImageView camImgView;
    MaterialEditText makeSelectBox, modelSelectBox, yearSelectBox, colorEditBox;
    String selected_make_code = "";
    String required_str = "";
    String imageType = "";
    String isFrom = "";
    Dialog uploadServicePicAlertBox = null;
    ArrayList<String> dataList = new ArrayList<>();
    android.support.v7.app.AlertDialog list_make;
    android.support.v7.app.AlertDialog list_model;
    android.support.v7.app.AlertDialog list_year;
    String iSelectedMakeId = "";
    String iSelectedModelId = "";
    int iSelectedMakePosition = 0;
    JSONArray year_arr;
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tow_truck_add_vehicle);
        generalFunc = new GeneralFunctions(getActContext());

        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        backImgView = (ImageView) findViewById(R.id.backImgView);
        camImgView = (ImageView) findViewById(R.id.camImgVIew);

        submitVehicleBtn = ((MaterialRippleLayout) findViewById(R.id.btn_type2)).getChildView();

        makeSelectBox = (MaterialEditText) findViewById(R.id.makeSelectBox);
        modelSelectBox = (MaterialEditText) findViewById(R.id.modelSelectBox);
        yearSelectBox = (MaterialEditText) findViewById(R.id.yearSelectBox);
        colorEditBox = (MaterialEditText) findViewById(R.id.colorEditBox);

        submitVehicleBtn.setId(Utils.generateViewId());
        submitVehicleBtn.setBackgroundColor(getResources().getColor(R.color.appThemeColor_1));
        submitVehicleBtn.setText(generalFunc.retrieveLangLBl("", "LBL_BTN_SUBMIT_TXT"));
        camImgView.setOnClickListener(new setOnClickList());

        backImgView.setOnClickListener(new setOnClickList());
        submitVehicleBtn.setOnClickListener(new setOnClickList());

        titleTxt.setText(generalFunc.retrieveLangLBl("Add Vehicle", "LBL_ADD_VEHICLE"));

        removeInput();
        setLabals();
        buildMakeList();

    }


    private void buildMakeList() {
        dataList.clear();
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "getUserVehicleDetails");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {
                    dataList.clear();
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {

                        JSONObject message_obj = generalFunc.getJsonObject("message", responseString);
                        year_arr = generalFunc.getJsonArray("year", message_obj.toString());

                        JSONArray carList_arr;
                        if (message_obj.length() > 0 && message_obj != null) {
                            carList_arr = generalFunc.getJsonArray("carlist", message_obj.toString());

                            if (carList_arr != null) {
                                for (int i = 0; i < carList_arr.length(); i++) {

                                    JSONObject obj = generalFunc.getJsonObject(carList_arr, i);
                                    dataList.add(obj.toString());
                                }
                            }
                        }

                    } else {
                        generalFunc.showGeneralMessage("",
                                generalFunc.retrieveLangLBl("", generalFunc.getJsonValue(CommonUtilities.message_str, responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }

    public String getSelectLangText() {
        return ("" + generalFunc.retrieveLangLBl("Select", "LBL_SELECT_LANGUAGE_HINT_TXT"));
    }


    private void setLabals() {
        titleTxt.setText(generalFunc.retrieveLangLBl("Add Your Vehicle", "LBL_ADD_VEHICLE_TXT"));

        makeSelectBox.setBothText(generalFunc.retrieveLangLBl("Make", "LBL_MAKE_TXT"));
        yearSelectBox.setBothText(generalFunc.retrieveLangLBl("Year", "LBL_YEAR_TXT"));
        modelSelectBox.setBothText(generalFunc.retrieveLangLBl("Model", "LBL_MODEL_TXT"));
        colorEditBox.setBothText(generalFunc.retrieveLangLBl("Color", "LBL_COLOR_TXT"));

        makeSelectBox.getLabelFocusAnimator().start();
        yearSelectBox.getLabelFocusAnimator().start();
        modelSelectBox.getLabelFocusAnimator().start();
        colorEditBox.getLabelFocusAnimator().start();
    }

    private void removeInput() {
        Utils.removeInput(makeSelectBox);
        Utils.removeInput(modelSelectBox);
        Utils.removeInput(yearSelectBox);

        makeSelectBox.setOnTouchListener(new SetOnTouchList());
        modelSelectBox.setOnTouchListener(new SetOnTouchList());
        yearSelectBox.setOnTouchListener(new SetOnTouchList());

        makeSelectBox.setOnClickListener(new setOnClickList());
        modelSelectBox.setOnClickListener(new setOnClickList());
        yearSelectBox.setOnClickListener(new setOnClickList());
    }

    public Context getActContext() {
        return TowTruckAddVehicleActivity.this;
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    public File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void chooseFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public View getCurrView() {
        return generalFunc.getCurrentView(TowTruckAddVehicleActivity.this);
    }

    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public void chooseFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void addVehicle() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "UpdateUserVehicleDetails");
        parameters.put("iUserId", generalFunc.getMemberId());
        parameters.put("iMakeId", iSelectedMakeId);
        parameters.put("iModelId", iSelectedModelId);
        parameters.put("iYear", yearSelectBox.getText().toString());
        parameters.put("vColour", colorEditBox.getText().toString());

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {

                if (responseString != null && !responseString.equals("")) {

                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);

                    if (isDataAvail == true) {

                        (new StartActProcess(getActContext())).setOkResult();

                        backImgView.performClick();
                    } else {
                        generalFunc.showGeneralMessage("",
                                generalFunc.retrieveLangLBl("", generalFunc.getJsonValue(CommonUtilities.message_str, responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();

    }

    private void showMakeList() {

        ArrayList<String> items = new ArrayList<String>();

        for (int i = 0; i < dataList.size(); i++) {
            items.add(generalFunc.getJsonValue("vMake", dataList.get(i)));
        }

        CharSequence[] cs_currency_txt = items.toArray(new CharSequence[items.size()]);


        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
        builder.setTitle(generalFunc.retrieveLangLBl("Select Make", "LBL_SELECT_MAKE"));

        builder.setItems(cs_currency_txt, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection

                if (list_make != null) {
                    list_make.dismiss();
                }
                makeSelectBox.setText(generalFunc.getJsonValue("vMake", dataList.get(item)));
                iSelectedMakeId = generalFunc.getJsonValue("iMakeId", dataList.get(item));
                iSelectedMakePosition = item;
                iSelectedModelId = "";
                modelSelectBox.setText("");

            }
        });

        list_make = builder.create();

        if (generalFunc.isRTLmode() == true) {
            generalFunc.forceRTLIfSupported(list_make);
        }

        list_make.show();
    }

    private void buildYear() {
//        if (year_arr == null || year_arr.length() == 0) {
//            return;
//        }
//
//        ArrayList<String> items = new ArrayList<String>();
//
//        for (int i = 0; i < year_arr.length(); i++) {
//            items.add((String) generalFunc.getValueFromJsonArr(year_arr, i));
//        }
//
//        CharSequence[] cs_currency_txt = items.toArray(new CharSequence[items.size()]);
//
//
//        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
//        builder.setTitle(generalFunc.retrieveLangLBl("Select Year", "LBL_SELECT_YEAR"));
//
//        builder.setItems(cs_currency_txt, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int item) {
//                // Do something with the selection
//
//                if (list_year != null) {
//                    list_year.dismiss();
//                }
//                yearSelectBox.setText((String) generalFunc.getValueFromJsonArr(year_arr, item));
//
//            }
//        });
//
//        list_year = builder.create();
//
//        if (generalFunc.isRTLmode() == true) {
//            generalFunc.forceRTLIfSupported(list_make);
//        }
//
//        list_year.show();
    }

    private void buildModelList() {

        ArrayList<String> items = new ArrayList<String>();

        JSONArray vModellistArr = generalFunc.getJsonArray("vModellist", dataList.get(iSelectedMakePosition));
        if (vModellistArr != null) {
            for (int i = 0; i < vModellistArr.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(vModellistArr, i);

                items.add(generalFunc.getJsonValue("vTitle", obj_temp.toString()));
            }

            CharSequence[] cs_currency_txt = items.toArray(new CharSequence[items.size()]);

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
            builder.setTitle(generalFunc.retrieveLangLBl("Select Models", "LBL_SELECT_MODEL"));

            builder.setItems(cs_currency_txt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection

                    if (list_make != null) {
                        list_make.dismiss();
                    }
                    JSONArray vModellistArr = generalFunc.getJsonArray("vModellist", dataList.get(iSelectedMakePosition));
                    JSONObject obj_temp = generalFunc.getJsonObject(vModellistArr, item);

                    modelSelectBox.setText(generalFunc.getJsonValue("vTitle", obj_temp.toString()));
                    iSelectedModelId = generalFunc.getJsonValue("iModelId", obj_temp.toString());
                }
            });

            list_model = builder.create();

            if (generalFunc.isRTLmode() == true) {
                generalFunc.forceRTLIfSupported(list_model);
            }

            list_model.show();
        }

    }

    class ImageSourceDialog implements Runnable {

        @Override
        public void run() {

            final Dialog dialog_img_update = new Dialog(getActContext(), R.style.ImageSourceDialogStyle);

            dialog_img_update.setContentView(R.layout.design_image_source_select);

            MTextView chooseImgHTxt = (MTextView) dialog_img_update.findViewById(R.id.chooseImgHTxt);
            chooseImgHTxt.setText(generalFunc.retrieveLangLBl("Choose Category", "LBL_CHOOSE_CATEGORY"));

            SelectableRoundedImageView cameraIconImgView = (SelectableRoundedImageView) dialog_img_update.findViewById(R.id.cameraIconImgView);
            SelectableRoundedImageView galleryIconImgView = (SelectableRoundedImageView) dialog_img_update.findViewById(R.id.galleryIconImgView);

            ImageView closeDialogImgView = (ImageView) dialog_img_update.findViewById(R.id.closeDialogImgView);

            closeDialogImgView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (dialog_img_update != null) {
                        dialog_img_update.cancel();
                    }
                }
            });

            new CreateRoundedView(getResources().getColor(R.color.appThemeColor_Dark_1), Utils.dipToPixels(getActContext(), 25), 0,
                    Color.parseColor("#00000000"), cameraIconImgView);

            cameraIconImgView.setColorFilter(getResources().getColor(R.color.appThemeColor_TXT_1));

            new CreateRoundedView(getResources().getColor(R.color.appThemeColor_Dark_1), Utils.dipToPixels(getActContext(), 25), 0,
                    Color.parseColor("#00000000"), galleryIconImgView);

            galleryIconImgView.setColorFilter(getResources().getColor(R.color.appThemeColor_TXT_1));

            cameraIconImgView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (dialog_img_update != null) {
                        dialog_img_update.cancel();
                    }
                    if (!isDeviceSupportCamera()) {
                        generalFunc.showMessage(getCurrView(), generalFunc.retrieveLangLBl("", "LBL_NOT_SUPPORT_CAMERA_TXT"));
                    } else {
                        chooseFromCamera();
                    }
                }
            });

            galleryIconImgView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (dialog_img_update != null) {
                        dialog_img_update.cancel();
                    }
                    chooseFromGallery();
                }
            });

            dialog_img_update.setCanceledOnTouchOutside(true);
            Window window = dialog_img_update.getWindow();
            window.setGravity(Gravity.BOTTOM);
            window.setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog_img_update.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog_img_update.show();

        }
    }

    public class setOnClickList implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            int i = v.getId();
            Utils.hideKeyboard(getActContext());
            if (i == R.id.backImgView) {
                TowTruckAddVehicleActivity.super.onBackPressed();
            } else if (i == submitVehicleBtn.getId()) {
                addVehicle();
            } else if (i == R.id.makeSelectBox) {
                if (list_make == null) {
                    showMakeList();

                } else {
                    list_make.show();
                }
            } else if (i == R.id.modelSelectBox) {
                buildModelList();
            } else if (i == R.id.yearSelectBox) {
                if (list_year == null) {
                    buildYear();

                } else {
                    list_year.show();
                }
            } else if (i == R.id.camImgVIew) {
                if (generalFunc.isCameraStoragePermissionGranted()) {
                    new ImageSourceDialog().run();
                } else {
                    generalFunc.showMessage(getCurrView(), "Allow this app to use camera.");
                }
            }
        }
    }

}
