package com.cabu.rider;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;

import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.StartActProcess;
import com.cabu.rider.R;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.MButton;
import com.view.MTextView;
import com.view.MaterialRippleLayout;
import com.view.editBox.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EnterDeliveryDetailsActivity extends AppCompatActivity {

    public static String RECEIVER_NAME_KEY = "RECEIVER_NAME";
    public static String RECEIVER_MOBILE_KEY = "RECEIVER_MOBILE";
    public static String PICKUP_INS_KEY = "PICKUP_INS";
    public static String DELIVERY_INS_KEY = "DELIVERY_INS";
    public static String PACKAGE_DETAILS_KEY = "PACKAGE_DETAILS";
    public static String PACKAGE_TYPE_NAME_KEY = "PACKAGE_TYPE_NAME";
    public static String PACKAGE_TYPE_ID_KEY = "PACKAGE_TYPE_ID";

    MTextView titleTxt;
    ImageView backImgView;

    GeneralFunctions generalFunc;

    MaterialEditText receiverNameEditBox;
    MaterialEditText receiverMobileEditBox;
    MaterialEditText pickUpInstructionEditBox;
    MaterialEditText deliveryInstructionEditBox;
    MaterialEditText packageTypeBox;
    MaterialEditText packageDetailsEditBox;

    MButton btn_type2;

    String required_str = "";

    android.support.v7.app.AlertDialog alert_packageTypes;

    ArrayList<String[]> list_packageType_items = new ArrayList<>();

    String currentPackageTypeId = "";
    private String userProfileJson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_delivery_details);

        generalFunc = new GeneralFunctions(getActContext());
        userProfileJson = generalFunc.retrieveValue(CommonUtilities.USER_PROFILE_JSON);

        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        backImgView = (ImageView) findViewById(R.id.backImgView);

        receiverNameEditBox = (MaterialEditText) findViewById(R.id.receiverNameEditBox);
        receiverMobileEditBox = (MaterialEditText) findViewById(R.id.receiverMobileEditBox);
        pickUpInstructionEditBox = (MaterialEditText) findViewById(R.id.pickUpInstructionEditBox);
        deliveryInstructionEditBox = (MaterialEditText) findViewById(R.id.deliveryInstructionEditBox);
        packageTypeBox = (MaterialEditText) findViewById(R.id.packageTypeBox);
        packageDetailsEditBox = (MaterialEditText) findViewById(R.id.packageDetailsEditBox);

        btn_type2 = ((MaterialRippleLayout) findViewById(R.id.btn_type2)).getChildView();
        btn_type2.setBackgroundColor(getResources().getColor(R.color.appThemeColor_1));
        btn_type2.setId(Utils.generateViewId());

        receiverMobileEditBox.setInputType(InputType.TYPE_CLASS_NUMBER);

        receiverNameEditBox.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        receiverMobileEditBox.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        pickUpInstructionEditBox.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        deliveryInstructionEditBox.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        packageDetailsEditBox.setImeOptions(EditorInfo.IME_ACTION_DONE);

        Utils.removeInput(packageTypeBox);

        backImgView.setOnClickListener(new setOnClickList());
        btn_type2.setOnClickListener(new setOnClickList());
        packageTypeBox.setOnTouchListener(new setOnTouchList());
        packageTypeBox.setOnClickListener(new setOnClickList());

        setLabels();
    }

    public void setLabels() {
        titleTxt.setText(generalFunc.retrieveLangLBl("Delivery Details", "LBL_DELIVERY_DETAILS"));

        receiverNameEditBox.setBothText(generalFunc.retrieveLangLBl("Receiver Name", "LBL_RECEIVER_NAME"));
        receiverMobileEditBox.setBothText(generalFunc.retrieveLangLBl("Receiver Mobile", "LBL_RECEIVER_MOBILE"));
        pickUpInstructionEditBox.setBothText(generalFunc.retrieveLangLBl("Pickup instruction", "LBL_PICK_UP_INS"));
        deliveryInstructionEditBox.setBothText(generalFunc.retrieveLangLBl("Delivery instruction", "LBL_DELIVERY_INS"));
        packageTypeBox.setBothText(generalFunc.retrieveLangLBl("Package Type", "LBL_PACKAGE_TYPE"),
                generalFunc.retrieveLangLBl("Select package type", "LBL_SELECT_PACKAGE_TYPE"));
        packageDetailsEditBox.setBothText(generalFunc.retrieveLangLBl("Package Details", "LBL_PACKAGE_DETAILS"));

        required_str = generalFunc.retrieveLangLBl("", "LBL_FEILD_REQUIRD_ERROR_TXT");

        if (getIntent().getStringExtra("isDeliverNow") != null && getIntent().getStringExtra("isDeliverNow").equals("true")) {
            btn_type2.setText(generalFunc.retrieveLangLBl("Deliver Now", "LBL_DELIVER_NOW"));
//            btn_type2.setText(generalFunc.retrieveLangLBl("Send Request", "LBL_SEND_REQ"));
        } else {
            btn_type2.setText(generalFunc.retrieveLangLBl("Send Request", "LBL_CONFIRM_BOOKING"));
        }
    }

    public Context getActContext() {
        return EnterDeliveryDetailsActivity.this;
    }

    public void loadPackageTypes() {

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "loadPackageTypes");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);
        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);
                    if (isDataAvail == true) {
                        buildPackageTypes(generalFunc.getJsonValue(CommonUtilities.message_str, responseString));
                    } else {
                        generalFunc.showGeneralMessage("",
                                generalFunc.retrieveLangLBl("", generalFunc.getJsonValue(CommonUtilities.message_str, responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void buildPackageTypes(String message) {
        list_packageType_items.clear();
        ArrayList<String> items = new ArrayList<>();
        JSONArray arr_data = generalFunc.getJsonArray(message);
        if (arr_data != null) {
            for (int i = 0; i < arr_data.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(arr_data, i);
                String[] arr_str_data = new String[2];
                arr_str_data[0] = generalFunc.getJsonValue("iPackageTypeId", obj_temp.toString());
                arr_str_data[1] = generalFunc.getJsonValue("vName", obj_temp.toString());

                list_packageType_items.add(arr_str_data);
                items.add(generalFunc.getJsonValue("vName", obj_temp.toString()));
            }
            CharSequence[] cs_currency_txt = items.toArray(new CharSequence[items.size()]);

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
            builder.setTitle(generalFunc.retrieveLangLBl("Select package type", "LBL_SELECT_PACKAGE_TYPE"));
            builder.setItems(cs_currency_txt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection
                    if (alert_packageTypes != null) {
                        alert_packageTypes.dismiss();
                    }

                    String selectedPackageTypeId = list_packageType_items.get(item)[0];
                    currentPackageTypeId = selectedPackageTypeId;
                    packageTypeBox.setText(list_packageType_items.get(item)[1]);
                }
            });


            alert_packageTypes = builder.create();
            if (generalFunc.isRTLmode() == true) {
                generalFunc.forceRTLIfSupported(alert_packageTypes);
            }
            showPackageTypes();
        }
    }

    public void showPackageTypes() {
        if (alert_packageTypes != null) {
            alert_packageTypes.show();
        }
    }

    public void checkDetails() {
        boolean receiverNameEntered = Utils.checkText(receiverNameEditBox) ? true : Utils.setErrorFields(receiverNameEditBox, required_str);
        boolean receiverMobileEntered = Utils.checkText(receiverMobileEditBox) ? true : Utils.setErrorFields(receiverMobileEditBox, required_str);
        boolean pickUpInsEntered = Utils.checkText(pickUpInstructionEditBox) ? true : Utils.setErrorFields(pickUpInstructionEditBox, required_str);
        boolean deliveryInsEntered = Utils.checkText(deliveryInstructionEditBox) ? true : Utils.setErrorFields(deliveryInstructionEditBox, required_str);
        boolean packageDetailsEntered = Utils.checkText(packageDetailsEditBox) ? true : Utils.setErrorFields(packageDetailsEditBox, required_str);
        boolean packageTypeSelected = !currentPackageTypeId.trim().equals("") ? true : Utils.setErrorFields(packageTypeBox, required_str);

        if (receiverNameEntered == false || receiverMobileEntered == false || pickUpInsEntered == false || deliveryInsEntered == false
                || packageDetailsEntered == false || packageTypeSelected == false) {
            return;
        }

        Bundle data = new Bundle();
        data.putString(RECEIVER_NAME_KEY, Utils.getText(receiverNameEditBox));
        data.putString(RECEIVER_MOBILE_KEY, Utils.getText(receiverMobileEditBox));
        data.putString(PICKUP_INS_KEY, Utils.getText(pickUpInstructionEditBox));
        data.putString(DELIVERY_INS_KEY, Utils.getText(deliveryInstructionEditBox));
        data.putString(PACKAGE_DETAILS_KEY, Utils.getText(packageDetailsEditBox));
        data.putString(PACKAGE_TYPE_ID_KEY, currentPackageTypeId);
        data.putString(PACKAGE_TYPE_NAME_KEY, Utils.getText(packageTypeBox));

        (new StartActProcess(getActContext())).setOkResult(data);

        backImgView.performClick();
    }

    public class setOnTouchList implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && !view.hasFocus()) {
                view.performClick();
            }
            return true;
        }
    }

    public class setOnClickList implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int i = view.getId();
            Utils.hideKeyboard(getActContext());
            if (i == R.id.backImgView) {
                EnterDeliveryDetailsActivity.super.onBackPressed();
            } else if (i == btn_type2.getId()) {
                checkDetails();
            } else if (i == R.id.packageTypeBox) {
                if (alert_packageTypes != null) {
                    showPackageTypes();
                } else {
                    loadPackageTypes();
                }
            }
        }
    }
}
