package com.cabu.rider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;

import com.general.files.ExecuteWebServerUrl;
import com.general.files.GeneralFunctions;
import com.general.files.SetOnTouchList;
import com.general.files.StartActProcess;
import com.model.DeliveryDetails;
import com.cabu.rider.R;
import com.utils.CommonUtilities;
import com.utils.Utils;
import com.view.GenerateAlertBox;
import com.view.MButton;
import com.view.MTextView;
import com.view.MaterialRippleLayout;
import com.view.editBox.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

public class EnterMultiDeliveryDetailActivity extends AppCompatActivity {

    public String userProfileJson = "";
    public boolean isRecipientSelected = false;
    MTextView titleTxt;
    ImageView backImgView;
    String vRecipientId = "";
    String oldRecipientId = "";
    String vRecipientName = "";
    String vRecipientAddress = "";
    String vRecipientEmail = "";
    String vRecipientPhone = "";
    String vRecipientPhoneCode = "";
    String vPackageTypeName = "";
    GeneralFunctions generalFunc;
    MaterialEditText selectRecipientEditBox;
    MaterialEditText additionalDetailsEditBox;
    MaterialEditText packageTypeBox;
    MaterialEditText deliveryInstructionsEditBox;
    MButton btn_type2;
    String required_str = "";
    ArrayList<DeliveryDetails> list = new ArrayList<DeliveryDetails>();
    android.support.v7.app.AlertDialog alert_packageTypes;
    ArrayList<String[]> list_packageType_items = new ArrayList<>();
    String currentPackageTypeId = "";
    DeliveryDetails selectedDeliveryDetails;
    private String recipientvLatitude, recipientvLongitude;
    private boolean recipientExist = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_delivery_detail);

        generalFunc = new GeneralFunctions(getActContext());

        userProfileJson = getIntent().getStringExtra("UserProfileJson");
        list = (ArrayList<DeliveryDetails>) getIntent().getSerializableExtra("list");
        selectedDeliveryDetails = (DeliveryDetails) getIntent().getSerializableExtra("SelectedDetails");
        titleTxt = (MTextView) findViewById(R.id.titleTxt);
        backImgView = (ImageView) findViewById(R.id.backImgView);

        selectRecipientEditBox = (MaterialEditText) findViewById(R.id.selectRecipientEditBox);
        additionalDetailsEditBox = (MaterialEditText) findViewById(R.id.additionalDetailsEditBox);
        deliveryInstructionsEditBox = (MaterialEditText) findViewById(R.id.deliveryInstructionsEditBox);
        packageTypeBox = (MaterialEditText) findViewById(R.id.packageTypeBox);

        additionalDetailsEditBox.setSingleLine(false);
        additionalDetailsEditBox.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        additionalDetailsEditBox.setGravity(Gravity.TOP);

        deliveryInstructionsEditBox.setSingleLine(false);
        deliveryInstructionsEditBox.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        deliveryInstructionsEditBox.setGravity(Gravity.TOP);

        btn_type2 = ((MaterialRippleLayout) findViewById(R.id.btn_type2)).getChildView();
        btn_type2.setBackgroundColor(getResources().getColor(R.color.appThemeColor_1));
        btn_type2.setId(Utils.generateViewId());

        selectRecipientEditBox.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        deliveryInstructionsEditBox.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        additionalDetailsEditBox.setImeOptions(EditorInfo.IME_ACTION_DONE);

        backImgView.setOnClickListener(new setOnClickList());
        btn_type2.setOnClickListener(new setOnClickList());
        packageTypeBox.setOnTouchListener(new setOnTouchList());
        packageTypeBox.setOnClickListener(new setOnClickList());

        removeInput();
        setLabels();
        selectRecipientEditBox.setShowClearButton(false);
    }

    public void removeInput() {
        Utils.removeInput(selectRecipientEditBox);
        selectRecipientEditBox.setOnTouchListener(new SetOnTouchList());
        selectRecipientEditBox.setOnClickListener(new setOnClickList());
    }

    public void setLabels() {
        titleTxt.setText(generalFunc.retrieveLangLBl("", "LBL_RIDE_SHIPMENT_DETAIL_TXT"));

        selectRecipientEditBox.setBothText(generalFunc.retrieveLangLBl("", "LBL_SELECT_RECIPIENT_TXT"));
        additionalDetailsEditBox.setBothText(generalFunc.retrieveLangLBl("Additional Notes", "LBL_ADDITIONAL_NOTES"));

        deliveryInstructionsEditBox.setBothText(generalFunc.retrieveLangLBl("Delivery instruction", "LBL_DELIVERY_INS"));
        packageTypeBox.setBothText(generalFunc.retrieveLangLBl("Package Type", "LBL_PACKAGE_TYPE"),
                generalFunc.retrieveLangLBl("Select package type", "LBL_SELECT_PACKAGE_TYPE"));
        btn_type2.setText(generalFunc.retrieveLangLBl("Submit", "LBL_BTN_SUBMIT_TXT"));

        required_str = generalFunc.retrieveLangLBl("", "LBL_FEILD_REQUIRD_ERROR_TXT");

        if (selectedDeliveryDetails != null) {
            oldRecipientId = selectedDeliveryDetails.getRecipientId();
            vRecipientId = selectedDeliveryDetails.getRecipientId();
            vRecipientName = selectedDeliveryDetails.getRecipientName();
            vRecipientEmail = selectedDeliveryDetails.getRecipientEmailAddress();
            vRecipientPhoneCode = selectedDeliveryDetails.getRecipientPhoneCode();
            vRecipientPhone = selectedDeliveryDetails.getRecipientPhoneNumber();
            recipientvLatitude = selectedDeliveryDetails.getRecipientvLatitude();
            recipientvLongitude = selectedDeliveryDetails.getRecipientvLongitude();
            vPackageTypeName = selectedDeliveryDetails.getvPackageTypeName();
            currentPackageTypeId = selectedDeliveryDetails.getPackageTypeId();
            double lattitude = generalFunc.parseDoubleValue(0.0, recipientvLatitude);
            double longitude = generalFunc.parseDoubleValue(0.0, recipientvLongitude);
            if (lattitude != 0.0 && longitude != 0.0) {
                vRecipientAddress = selectedDeliveryDetails.getRecipientAddress();
            }
            selectRecipientEditBox.setText("" + vRecipientName);
            packageTypeBox.setText("" + vPackageTypeName);
            isRecipientSelected = true;
            deliveryInstructionsEditBox.setText("" + selectedDeliveryDetails.getShippmentDetailTxt());
            additionalDetailsEditBox.setText("" + selectedDeliveryDetails.getAdditionalNotes());
        }
    }

    public View getCurrView() {
        return generalFunc.getCurrentView(EnterMultiDeliveryDetailActivity.this);
    }

    public Context getActContext() {
        return EnterMultiDeliveryDetailActivity.this;
    }

    public void checkDetails() {
        boolean receiverNameEntered = isRecipientSelected ? true : Utils.setErrorFields(selectRecipientEditBox, required_str);
        boolean shipmentDetailsEntered = Utils.checkText(deliveryInstructionsEditBox) ? true : Utils.setErrorFields(deliveryInstructionsEditBox, required_str);
        boolean packageTypeSelected = (!TextUtils.isEmpty(currentPackageTypeId)) ? true : Utils.setErrorFields(packageTypeBox, required_str);

        if (receiverNameEntered == false || shipmentDetailsEntered == false || packageTypeSelected == false) {
            return;
        }

        if (selectedDeliveryDetails != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getRecipientId().equalsIgnoreCase(oldRecipientId)) {
                    list.set(i, addOrEditDetails());
                    break;
                }
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getRecipientId().equalsIgnoreCase(vRecipientId)) {
                    recipientExist = true;
                    recipientExist();
                    break;
                } else {
                    recipientExist = false;
                }
            }
            if (recipientExist == true) {
                return;
            } else {
                list.add(addOrEditDetails());
            }
        }

        if (recipientExist == true) {
            return;
        }

        Bundle bn = new Bundle();
        bn.putSerializable("list", list);

        (new StartActProcess(getActContext())).setOkResult(bn);
        finish();
    }

    private void recipientExist() {
        GenerateAlertBox generateAlertBox = new GenerateAlertBox(getActContext());
        generateAlertBox.setContentMessage("", generalFunc.retrieveLangLBl("", "LBL_SELECT_ANOTHER_RECIPIENT_LBL"));
        generateAlertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
            @Override
            public void handleBtnClick(int btn_id) {
            }
        });
        generateAlertBox.setNegativeBtn(generalFunc.retrieveLangLBl("Cancel", "LBL_BTN_CANCEL_TXT"));
        generateAlertBox.setPositiveBtn(generalFunc.retrieveLangLBl("Ok", "LBL_BTN_OK_TXT"));
        generateAlertBox.showAlertBox();
    }

    private DeliveryDetails addOrEditDetails() {
        DeliveryDetails deliveryDetails = new DeliveryDetails();
        deliveryDetails.setRecipientId("" + vRecipientId);
        deliveryDetails.setRecipientName("" + vRecipientName);
        deliveryDetails.setRecipientAddress("" + vRecipientAddress);
        deliveryDetails.setRecipientEmailAddress("" + vRecipientEmail);
        deliveryDetails.setRecipientPhoneNumber("" + vRecipientPhone);
        deliveryDetails.setRecipientPhoneCode("" + vRecipientPhoneCode);
        deliveryDetails.setAdditionalNotes("" + Utils.getText(additionalDetailsEditBox));
        deliveryDetails.setShippmentDetailTxt("" + Utils.getText(deliveryInstructionsEditBox));
        deliveryDetails.setYesLbl("" + generalFunc.retrieveLangLBl("", "LBL_BTN_YES_TXT"));
        deliveryDetails.setNoLbl("" + generalFunc.retrieveLangLBl("", "LBL_BTN_NO_TXT"));
        deliveryDetails.setRecipientvLatitude("" + recipientvLatitude);
        deliveryDetails.setRecipientvLongitude("" + recipientvLongitude);
        deliveryDetails.setPackageTypeId("" + currentPackageTypeId);
        deliveryDetails.setvPackageTypeName("" + vPackageTypeName);
        deliveryDetails.setDeleteDeliverDetailLbl("" + generalFunc.retrieveLangLBl("", "LBL_DELETE_DELIVERY_DETAILS_ALERT_TXT"));

        for (Field field : deliveryDetails.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            Object value = null;
            try {
                value = field.get(deliveryDetails);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return deliveryDetails;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.SELECT_RECIPIENT_REQ_CODE && resultCode == RESULT_OK && data != null) {
            recipientExist = false;
            currentPackageTypeId = data.getStringExtra("iPackageTypeId");
            vPackageTypeName = data.getStringExtra("vPackageTypeName");
            packageTypeBox.setText("" + vPackageTypeName);

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getRecipientId().equalsIgnoreCase(data.getStringExtra("recipientId"))) {
                    recipientExist = true;
                    break;
                }
            }
            if (recipientExist == true) {
                recipientExist();
                return;
            }

            vRecipientId = data.getStringExtra("recipientId");
            oldRecipientId = data.getStringExtra("oldRecipientId");
            vRecipientName = data.getStringExtra("recipientName");
            vRecipientAddress = data.getStringExtra("recipientAddress");
            vRecipientEmail = data.getStringExtra("recipientEmail");
            vRecipientPhone = data.getStringExtra("recipientPhone");
            vRecipientPhoneCode = data.getStringExtra("recipientPhoneCode");
            recipientvLatitude = data.getStringExtra("recipientvLatitude");
            recipientvLongitude = data.getStringExtra("recipientvLongitude");

            isRecipientSelected = true;
            selectRecipientEditBox.setText(vRecipientName);
        }
    }

    public void loadPackageTypes() {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("type", "loadPackageTypes");

        ExecuteWebServerUrl exeWebServer = new ExecuteWebServerUrl(getActContext(), parameters);
        exeWebServer.setLoaderConfig(getActContext(), true, generalFunc);

        exeWebServer.setDataResponseListener(new ExecuteWebServerUrl.SetDataResponse() {
            @Override
            public void setResponse(String responseString) {
                if (responseString != null && !responseString.equals("")) {
                    boolean isDataAvail = GeneralFunctions.checkDataAvail(CommonUtilities.action_str, responseString);
                    if (isDataAvail == true) {
                        buildPackageTypes(generalFunc.getJsonValue(CommonUtilities.message_str, responseString));
                    } else {
                        generalFunc.showGeneralMessage("",
                                generalFunc.retrieveLangLBl("", generalFunc.getJsonValue(CommonUtilities.message_str, responseString)));
                    }
                } else {
                    generalFunc.showError();
                }
            }
        });
        exeWebServer.execute();
    }

    public void buildPackageTypes(String message) {
        list_packageType_items.clear();

        ArrayList<String> items = new ArrayList<>();
        JSONArray arr_data = generalFunc.getJsonArray(message);
        if (arr_data != null) {
            for (int i = 0; i < arr_data.length(); i++) {
                JSONObject obj_temp = generalFunc.getJsonObject(arr_data, i);

                String[] arr_str_data = new String[2];
                arr_str_data[0] = generalFunc.getJsonValue("iPackageTypeId", obj_temp.toString());
                arr_str_data[1] = generalFunc.getJsonValue("vName", obj_temp.toString());

                list_packageType_items.add(arr_str_data);
                items.add(generalFunc.getJsonValue("vName", obj_temp.toString()));
            }

            CharSequence[] cs_currency_txt = items.toArray(new CharSequence[items.size()]);

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActContext());
            builder.setTitle(generalFunc.retrieveLangLBl("Select package type", "LBL_SELECT_PACKAGE_TYPE"));
            builder.setItems(cs_currency_txt, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    // Do something with the selection

                    if (alert_packageTypes != null) {
                        alert_packageTypes.dismiss();
                    }

                    String selectedPackageTypeId = list_packageType_items.get(item)[0];
                    vPackageTypeName = list_packageType_items.get(item)[1];
                    currentPackageTypeId = selectedPackageTypeId;
                    packageTypeBox.setText(list_packageType_items.get(item)[1]);
                }
            });

            alert_packageTypes = builder.create();
            if (generalFunc.isRTLmode() == true) {
                generalFunc.forceRTLIfSupported(alert_packageTypes);
            }
            showPackageTypes();
        }
    }

    public void showPackageTypes() {
        if (alert_packageTypes != null) {
            alert_packageTypes.show();
        }
    }

    public class setOnTouchList implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && !view.hasFocus()) {
                view.performClick();
            }
            return true;
        }
    }

    public class setOnClickList implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int i = view.getId();
            Utils.hideKeyboard(getActContext());
            if (i == R.id.backImgView) {
                finish();
            } else if (i == R.id.selectRecipientEditBox) {
                Bundle bn = new Bundle();
                bn.putSerializable("list", list);
                bn.putString("iPackageTypeId", currentPackageTypeId);
                bn.putString("vPackageTypeName", vPackageTypeName);
                bn.putString("oldRecipientId", oldRecipientId);
                new StartActProcess(getActContext()).startActForResult(RecipientListActivity.class, bn, Utils.SELECT_RECIPIENT_REQ_CODE);
            } else if (i == btn_type2.getId()) {
                checkDetails();
            } else if (i == R.id.packageTypeBox) {
                if (alert_packageTypes != null) {
                    showPackageTypes();
                } else {
                    loadPackageTypes();
                }
            }
        }
    }
}
