package com.general.files;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.cabu.rider.ChatActivity;
import com.cabu.rider.MainActivity;
import com.cabu.rider.OnGoingTripDetailsActivity;
import com.utils.CommonUtilities;

/**
 * Created by Admin on 12-07-2016.
 */
public class GcmBroadCastReceiver extends BroadcastReceiver {
    MainActivity mainAct;
    ChatActivity chatAct;
    OnGoingTripDetailsActivity onGoingTripDetailsAct;

    public GcmBroadCastReceiver(MainActivity mainAct) {
        this.mainAct = mainAct;
    }

    public GcmBroadCastReceiver(OnGoingTripDetailsActivity onGoingTripDetailsAct) {
        this.onGoingTripDetailsAct = onGoingTripDetailsAct;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(CommonUtilities.driver_message_arrived_intent_action)) {
            String message = intent.getExtras().getString(CommonUtilities.driver_message_arrived_intent_key);
            if (mainAct != null) {
                mainAct.onGcmMessageArrived(message);
            }
            if (onGoingTripDetailsAct != null) {
                onGoingTripDetailsAct.onGcmMessageArrived(message);
            }

        }
    }
}
