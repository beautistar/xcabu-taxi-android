package com.adapter.files;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.general.files.GeneralFunctions;
import com.model.DeliveryDetails;

import com.cabu.rider.MultiDeliverDeliveryListActivity;
import com.cabu.rider.R;
import com.utils.Utils;
import com.view.CreateRoundedView;
import com.view.GenerateAlertBox;
import com.view.MTextView;

import java.util.ArrayList;

/**
 * Created by Admin on 09-07-2016.
 */
public class MultiDeliverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    public GeneralFunctions generalFunc;
    ArrayList<DeliveryDetails> list;
    Context mContext;
    boolean isFooterEnabled = false;
    View footerView;
    FooterViewHolder footerHolder;
    MultiDeliverDeliveryListActivity multiDeliverDeliveryListActivity;
    private OnItemClickListener mItemClickListener;


    public MultiDeliverAdapter(Context mContext, ArrayList<DeliveryDetails> list, GeneralFunctions generalFunc, boolean isFooterEnabled) {
        this.mContext = mContext;
        this.list = list;
        this.generalFunc = generalFunc;
        this.isFooterEnabled = isFooterEnabled;
        multiDeliverDeliveryListActivity = (MultiDeliverDeliveryListActivity) mContext;
    }

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(mContext).inflate(R.layout.footer_list, parent, false);
            this.footerView = v;
            return new FooterViewHolder(v);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_multi_deliver_details_design, parent, false);
            return new ViewHolder(view);
        }

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof ViewHolder) {
            final DeliveryDetails item = list.get(position);
            final ViewHolder viewHolder = (ViewHolder) holder;

            viewHolder.deliverNameTxt.setText(item.getRecipientName());
            viewHolder.deliverAddressTxt.setText(item.getRecipientAddress());

            new CreateRoundedView(Color.parseColor("#ffffff"), Utils.dipToPixels(mContext, 8), Utils.dipToPixels(mContext, 1),
                    Color.parseColor("#CECECE"), viewHolder.contentArea);

            viewHolder.deleteBtnArea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GenerateAlertBox generateAlertBox = new GenerateAlertBox(mContext);
                    generateAlertBox.setContentMessage("", item.getDeleteDeliverDetailLbl());

                    generateAlertBox.setBtnClickList(new GenerateAlertBox.HandleAlertBtnClick() {
                        @Override
                        public void handleBtnClick(int btn_id) {
                            if (btn_id == 1) {
                                multiDeliverDeliveryListActivity.deleteDeliverDetails(item.getRecipientId());
                            }
                        }
                    });
                    generateAlertBox.setPositiveBtn(item.getYesLbl());
                    generateAlertBox.setNegativeBtn(item.getNoLbl());
                    generateAlertBox.showAlertBox();
                }
            });

            viewHolder.editBtnArea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    multiDeliverDeliveryListActivity.editDeliveryDetails(item);
                }
            });
        } else {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            this.footerHolder = footerHolder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position) && isFooterEnabled == true) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionFooter(int position) {
        return position == list.size();
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (isFooterEnabled == true) {
            return list.size() + 1;
        } else {
            return list.size();
        }

    }

    public void addFooterView() {
        this.isFooterEnabled = true;
        notifyDataSetChanged();
        if (footerHolder != null)
            footerHolder.progressArea.setVisibility(View.VISIBLE);
    }

    public void removeFooterView() {
        if (footerHolder != null)
            footerHolder.progressArea.setVisibility(View.GONE);
    }

    public interface OnItemClickListener {
        void onItemClickList(View v, int position);
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {


        private LinearLayout editBtnArea;
        private LinearLayout deleteBtnArea;
        private MTextView deliverNameTxt;
        private MTextView deliverAddressTxt;
        private MTextView parcelWeight;
        private MTextView parcelWeightValTxt;
        private LinearLayout contentArea;

        public ViewHolder(View view) {
            super(view);

            editBtnArea = (LinearLayout) view.findViewById(R.id.editBtnArea);
            deleteBtnArea = (LinearLayout) view.findViewById(R.id.deleteBtnArea);
            deliverNameTxt = (MTextView) view.findViewById(R.id.deliverNameTxt);
            deliverAddressTxt = (MTextView) view.findViewById(R.id.deliverAddressTxt);
            parcelWeight = (MTextView) view.findViewById(R.id.parcelWeight);
            parcelWeightValTxt = (MTextView) view.findViewById(R.id.parcelWeightValTxt);
            contentArea = (LinearLayout) view.findViewById(R.id.contentArea);

        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        LinearLayout progressArea;

        public FooterViewHolder(View itemView) {
            super(itemView);

            progressArea = (LinearLayout) itemView;

        }
    }
}
